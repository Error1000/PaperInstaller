import os
import time

####################WARNING###################
#This should only be used on the arch linux iso
##############################################


###############HELPER FUNCTIONS###############
#Unsafe but helps with some commands
def systemRun(cmd):
    return os.system(cmd) >> 8

#Unsafe but helps with some commands
def systemRunAndGetOutput(cmd):
    return str(os.popen(cmd).read())

def runAsChRoot(cmd, root="/mnt"):
    cmd = str(cmd)
    root = str(root)

    cmd = cmd.replace("\"", "\\\"")

    return systemRun("arch-chroot " + root + " /bin/bash -c \"" + cmd + "\"")

def runAsChRootAndGetOutput(cmd, root="/mnt"):
    cmd = str(cmd)
    root = str(root)

    cmd = cmd.replace("\"", "\\\"")

    return str(systemRunAndGetOutput("arch-chroot " + root + " /bin/bash -c \"" + cmd + "\""))

def exitInstaller():
    systemRun("umount -R /mnt")
    print("Exiting installer ...")
    exit(0)

def amIRunningAsRoot():
    res = str(systemRunAndGetOutput("whoami"))
    res = res.replace("\n", "")
    res = res.replace(" ", "")
    return res == "root"

def isInternetAvailable(pingLen = 3, domain = "8.8.8.8"):
    access = False
    pingOut = systemRunAndGetOutput("ping -c " + str(pingLen) + " " + domain)
    for line in pingOut.split("\n"):
        sections = line.split(",")
        if sections.__len__() == 4:
            for section in sections:
                if section.__contains__("packet loss"):
                    section = section.strip()
                    precent = float(section.split(" ")[0].replace("%", "").strip())
                    access = (precent < 50.0)
    return access

def listPartitions(askUser=True):
    listParts = "y"
    if askUser:
        print("Would you like to list partitions and disks (you may be asked about information about your partitions and disks)? (y/n): ", end = '')
        listParts = str(input())
        print()
    if listParts != "n":
        systemRun("lsblk")

############################################


systemRun("clear")

print("Welcome to the PaperInstaller!")
print("Press any key to continue ...")
input()


#Global vars
#-------------------------------------------------------------
BOOT_MODE = 1  # 1 = BIOS, 2 = UEFI, assume BIOS by default
essentialAndCompatibilityApps = []
#-------------------------------------------------------------


#Root check
#-------------------------------------------------------------
if not amIRunningAsRoot():
    print("Please run this script as root on the arch iso!")
    exitInstaller()
#-------------------------------------------------------------


#Make sure no drives are mounted in /mnt
#-------------------------------------------------------------
print("Unmounting all drives mounted to /mnt or drives mounted to directories in /mnt ...")
systemRun("umount -R /mnt")
print("Done!")
#-------------------------------------------------------------


#Verify boot mode
#-------------------------------------------------------------
bootLsOutSuccess = systemRun("ls /sys/firmware/efi/efivars > /dev/null 2>&1")

if bootLsOutSuccess != 0:
    #Assumming BIOS
    BOOT_MODE = 1
else:
    #Assumming UEFI
    BOOT_MODE = 2
#-------------------------------------------------------------


#Check for internet access
#-------------------------------------------------------------
print("Checking for internet access ...")

hasAccess = isInternetAvailable()

if not hasAccess:
    print("Looks like you are nto connected to the internet.")
    print("Would you like to try to connect to the internet? (y/n): ", end='')
    ans = str(input())
    print()
    if ans == "y":
        print("Do you use wifi or are you wired?(y/n)(y = wifi, n = wired): ", end='')
        ans2 = str(input())
        print()

        if ans2 == "y":
            print("Prehaps you are not connected to a wifi network, would you like to open the cli program used to connect to one?(y/n): ", end='')
            ans3 = str(input())
            print()
            if ans3 == "y":
                systemRun("nmtui")
            else:
                exitInstaller()
        else:
            print("Prehaps the dhcpcd networking service is not running/working properly, would you like to start/restart it?(y/n): ", end='')
            ans3 = str(input())
            print()

            if ans3 == "y":
                systemRun("systemctl stop dhcpcd")
                systemRun("systemctl start dhcpcd")
            else:
                exitInstaller()

    print("Testing network connection ...")
    if isInternetAvailable():
        print("Looks like everything is alright, the fix worked! :)")
    else:
        print("Internet access could not be acquired initiating self-destruct sequence ... jk ;)")
        exitInstaller()
else:
    print("Looks like internet is working!")
#-------------------------------------------------------------



#Updating the live iso
#-------------------------------------------------------------
print("Updating installer ...")
systemRun("pacman --noconfirm -Syy")
print("Done!")
#-------------------------------------------------------------



#Update clock of the live iso
#-------------------------------------------------------------
print("Updating clock of the installer ...")
timeSuccess = systemRun("timedatectl set-ntp true")
if timeSuccess == 0:
    print("Done!")
else:
    print("Somehow the command to update the clock failed!")
    print("However this is not fatal so the installer will continue!")
#-------------------------------------------------------------



#For menu
nrOptions = 10
optionsDone = [False] * nrOptions

class Options:
    partitionAndFormatDisks, \
    mountPartitions, \
    updateMirrors, \
    installBase, \
    configureSystem, \
    configureLanguageAndTime, \
    personalizeSystem, \
    installExtra, \
    installBootloader, \
    chrootShell, \
    *_ = range(nrOptions)
    # NOTE: The *_ is to "soak up" extra variables produced by the range function


def getMarkAndNumberOf(opt):
    if optionsDone[opt]:
        return "[x] " + str(int(opt + 1)) + ") "
    else:
        return "[] " + str(int(opt + 1)) + ") "

while True:

    #Menu
    #-------------------------------------------------------------
    time.sleep(5)
    systemRun("clear")
    print(                                                         "-----------------------------------------------------------------------MAIN MENU-----------------------------------------------------------------------")
    print( getMarkAndNumberOf(Options.partitionAndFormatDisks  ) + "Partition and format disk.")
    print( getMarkAndNumberOf(Options.mountPartitions          ) + "Mount partitions.")
    print( getMarkAndNumberOf(Options.updateMirrors            ) + "Update installer mirrors. (optional)")
    print( getMarkAndNumberOf(Options.installBase              ) + "Install base system (essential apps, the kernel, drivers, etc.).")
    print( getMarkAndNumberOf(Options.configureSystem          ) + "Configure file system tab and sudoers file.")
    print( getMarkAndNumberOf(Options.configureLanguageAndTime ) + "Configure time zone and locale of installation.")
    print( getMarkAndNumberOf(Options.personalizeSystem        ) + "Set hostname, root password and add extra users.")
    print( getMarkAndNumberOf(Options.installExtra             ) + "Install extra useful apps like a network manager, and an aur manager. (optional)")
    print( getMarkAndNumberOf(Options.installBootloader        ) + "Install bootloader.")
    print( getMarkAndNumberOf(Options.chrootShell              ) + "Enter a chroot shell of the system to install or configure the system more, this option is great for rescuing a system that doesn't boot. (optional)")
    print(                                                         "-------------------------------------------------------------------------------------------------------------------------------------------------------")


    print("You should do these from the first to the last one, one by one if you wish to install arch, you can also omit the optional ones if you don't want to do them.")
    print("You can re-do sections like mounting and partitioning if you missed something but be careful, you can also skip sections even if they are not optional but again this is an advanced feature and should only be used if you know what you are doing!!!")
    print("Please choose and option (1-" + str(nrOptions) + "), or -1 to quit: ", end = '')
    option = int(input())
    print()
    systemRun("clear")

    if option == -1: break
    #The list is from 1 to nrOfOptions, but the array is from 0 to nrOfOptions-1, so to adapt we subtract 1
    option -= 1
    #-------------------------------------------------------------




    if option == Options.partitionAndFormatDisks:
        #Partition disk
        #-------------------------------------------------------------
        #TODO: Add auto partitioning and auto formatting
        listPartitions()

        #Choose disk
        print("What disk would you like to partition to install arch linux onto? (ex: /dev/sda): ", end = '')
        disk = str(input())
        print()

        print("Would you like to use auto-partitioning and auto-formatting? (y/n): ", end = '')
        autoFormat = str(input())
        print()

        if autoFormat == "y":
            diskInfo = systemRunAndGetOutput("fdisk " + disk + " -l").split("\n")
            diskSizeInfo = str(diskInfo[0].split(disk)[1])[1:].strip().split(",")
            diskSizeInGiB = int(diskSizeInfo[0].split(" ")[0])


        #Choose tool to partition with
        print("Would you like to use fdisk(terminal commands), or cfdisk(simple cli)? (y/n) (y = fdisk, n = cfdisk): ", end='')
        choice = str(input())
        print()

        if choice == "y":
            systemRun("fdisk " + disk)
        else:
            systemRun("cfdisk " + disk)

        #Clear console from the last tool
        systemRun("clear")

        #-------------------------------------------------------------




        #Format partitions
        #-------------------------------------------------------------
        listPartitions()

        print("What is the main(root) partition? (ex: /dev/sda1): ", end='')
        partitionToFormat = str(input())
        print()


        while True:

            nrOfFileSystems = 3

            print("1) ext4 (recommended)")
            print("2) ext3")
            print("3) ext2")
            print("Please choose a filesystem to format the partition with, or if you have chosen the wrong partition use -1 to get back to the main menu. (1-" + str(nrOfFileSystems) + "): ", end='')
            chosenFs = int(input())
            print()

            if chosenFs == -1:
                break

            if chosenFs > nrOfFileSystems:
                print(str(chosenFs) + " is not a valid choice!")
                continue

            filesys = ""
            if chosenFs == 1: filesys = "ext4"
            elif chosenFs == 2: filesys = "ext3"
            elif chosenFs == 3: filesys = "ext2"
            else:
                print("Option: " + str(chosenFs) + " has not been implemented yet!")
                continue


            #TODO: Add ntfs and fat support for other partitions that are not root, and even maybe ntfs support for the root partition
            print("Formatting " + partitionToFormat + " with filesystem: " + filesys)

            systemRun("mkfs." + filesys + " " + partitionToFormat)

            print("Done!")

            optionsDone[Options.partitionAndFormatDisks] = True

            print("Would you like to format any other partition? (y/n): ", end='')
            formatAns = str(input())
            print()

            if formatAns != "y": break

            listPartitions()

            print("What partition would you like to format? (ex: /dev/sda2): ", end='')
            partitionToFormat = str(input())
            print()

        print("Done!")
        #-------------------------------------------------------------


    elif option == Options.mountPartitions:
        #Mount partitions
        #-------------------------------------------------------------
        while True:
          if optionsDone[Options.mountPartitions]: break

          listPartitions()
          mountPointOfPartition = "/"
          #Choose partition
          print("Please enter the name of the main(root) partition (ex: /dev/sda1): ", end='')
          toMountPartition = str(input())
          print()

          # Mount partition
          systemRun("mkdir -p /mnt" + mountPointOfPartition)

          mountWorked = systemRun("mount " + toMountPartition + " /mnt" + mountPointOfPartition)

          if mountWorked != 0:
              print("Couldn't mount partition, are you sure it's the right one?")
              break
          else:
              optionsDone[Options.mountPartitions] = True
              break

        if not optionsDone[Options.mountPartitions]: continue

        while True:
           print("Would you like to add any other mount points? (y/n): ", end='')
           mountAnyOtherPoints = str(input())
           print()
           if mountAnyOtherPoints != "y": break
           listPartitions()
           print("What is the partition?(ex: /dev/sda2): ", end='')
           toMountPartition = str(input())
           print()
           print("Where would you like to mount the partition " + toMountPartition + " ?(ex: /home): ", end='')
           mountPointOfPartition = str(input())
           print()

           # Mount partition
           systemRun("mkdir -p /mnt" + mountPointOfPartition)
           mountWorked = systemRun("mount " + toMountPartition + " /mnt" + mountPointOfPartition)

           if mountWorked != 0:
               print("Couldn't mount partition, are you sure it's the right one?")

        print("Done!")
        #-------------------------------------------------------------




    elif option == Options.updateMirrors:
        #Update installer mirrors
        #-------------------------------------------------------------
        print("Installing mirror ranker ...")
        rankerInstallSuccess = systemRun("pacman --noconfirm -Sy reflector")
        if rankerInstallSuccess != 0:
            print("Couldn't install mirror ranker!")
            print("However ranking the mirrors is not required as it's an optional step, so the installer will continue")
            continue
        print("Ranking and updating mirrors ...")
        rankSuccess = systemRun("python /usr/lib/python*/site-packages/Reflector.py --verbose -l 5 --sort rate --save /etc/pacman.d/mirrorlist")
        if rankSuccess != 0:
            print("Ranker couldn't rank mirrors!")
            print("However ranking the mirrors is not required as it's an optional step, so the installer will continue")
            continue
        print("Done!")
        optionsDone[Options.updateMirrors] = True
        #-------------------------------------------------------------




    elif option == Options.installBase:
        #Install base system
        #-------------------------------------------------------------
        if not optionsDone[Options.mountPartitions]:
            print("Please mount the partitions first before installing the base system onto them!")
            continue

        #Extra packages
        print("Would you like to also install base-devel (useful for building from source and using the AUR)? (y/n): ", end='')
        useExtra = str(input())
        print()

        additional = "sudo "

        if useExtra == "y":
            additional += "base-devel "

        print("Installing base system ...")

        baseInstallSuccess = systemRun("pacstrap /mnt base " + additional)

        if baseInstallSuccess != 0:
            print("Failed to install base system!")
            exitInstaller()

        appsToInstall = ""
        for app in essentialAndCompatibilityApps:
            appsToInstall += app + " "

        systemRun("pacstrap /mnt " + appsToInstall)

        print("Done!")
        optionsDone[Options.installBase] = True
        #-------------------------------------------------------------




    elif option == Options.configureSystem:
        #Configure file system tab and edit the sudoers file
        #-------------------------------------------------------------


        #Configure file system tab
        #-------------------------------------------------------------
        if not optionsDone[Options.mountPartitions]:
            print("Please mount the partitions first, before generating the file system tab!")
            continue

        if not optionsDone[Options.installBase]:
            print("Please install the system first before generating the file system tab as not doing so could result in it being overridden!")
            continue

        print("Generating fstab ...")
        #Reset fstab file
        #FIXME: This used hardcoded file sizes for resetting the files
        systemRun("head -4 /mnt/etc/fstab > /mnt/etc/TempFstab")
        systemRun("cat /mnt/etc/TempFstab > /mnt/etc/fstab")
        systemRun("rm /mnt/etc/TempFstab")

        systemRun("genfstab -U /mnt >> /mnt/etc/fstab")

        while True:
            print("Would you like to see the /ect/fstab file? (y/n): ", end='')
            seeIt = str(input())
            print()
            if seeIt == "y":
                systemRun("cat /mnt/etc/fstab")
            else:
                break

            print("Would you like to edit the /etc/fstab file? (y/n): ", end='')
            editIt = str(input())
            print()
            if editIt == "y":
                systemRun("nano /mnt/etc/fstab")
            else:
                break
        #-------------------------------------------------------------


        #Edit the sudoers file
        #-------------------------------------------------------------
        print("Editing the sudoers file ...")
        #Reset sudoers file
        #FIXME: This used hardcoded file sizes for resetting the files
        runAsChRoot("head -97 /etc/sudoers > /etc/TempSudoers")
        runAsChRoot("cat /etc/TempSudoers > /etc/sudoers")
        runAsChRoot("rm /etc/TempSudoers")

        runAsChRoot("echo \"#This is here to give sudo access to all users in the wheel group!\" >> /etc/sudoers")
        runAsChRoot("echo \"%wheel ALL=(ALL) ALL\" >> /etc/sudoers")
        #-------------------------------------------------------------

        print("Done!")

        optionsDone[Options.configureSystem] = True
        #-------------------------------------------------------------




    elif option == Options.configureLanguageAndTime:
        #Set locale and time zone
        #-------------------------------------------------------------


        #Set time zone
        #-------------------------------------------------------------
        if not optionsDone[Options.mountPartitions]:
            print("Please mount the partitions first!")
            continue

        regionLs = "Note: use the down and up arrows to scroll this list and q to quit.\n"
        regionI = 0
        regions = runAsChRootAndGetOutput("ls /usr/share/zoneinfo").split("\n")
        for rI in range(regions.__len__()):
            if regions[rI] == "":
                regions.__delitem__(rI)

        for region in regions:
            regionI += 1
            regionLs += str(regionI) + ") " + region + "\n"
        while True:
          systemRun("echo '" + regionLs + "' | less")

          print("Please enter the number of the region you wish to use(1-" + str(regions.__len__()) + "), or -2 to list the regions again!: ", end='')
          regionResponse = int(input())
          print()
          if regionResponse < -2: continue
          if regionResponse != -2: break

        region = regions[regionResponse - 1].replace("\n", "").strip()


        zoneLs = "Note: use the down and up arrows to scroll this list and q to quit.\n"
        zoneI = 0
        zones = runAsChRootAndGetOutput("ls /usr/share/zoneinfo/" + region).split("\n")
        for zI in range(zones.__len__()):
            if zones[zI] == "":
                zones.__delitem__(zI)

        for zone in zones:
            zoneI += 1
            zoneLs += str(zoneI) + ") " + zone + "\n"
        while True:
            systemRun("echo '" + zoneLs + "' | less")

            print("Please enter the number of the zone you wish to use(1-" + str(zones.__len__()) + "), or -2 to list the zones again!: ", end='')
            zoneResponse = int(input())
            print()
            if zoneResponse < -2: continue
            if zoneResponse != -2: break

        zone = zones[zoneResponse - 1].replace("\n", "").strip()

        print("Setting time zone to: " + region + "/" + zone + " ...")
        runAsChRoot("ln -sf /usr/share/zoneinfo/" + region + "/" + zone + " /etc/localtime ")

        print("Would you like to use utc for the time( this is only really helpful when dual booting since windows uses utc and if this is not utc linux and windows will battle over the clock )? (y/n): ", end='')
        useUtc = str(input())
        if useUtc == "y":
          runAsChRoot("hwclock --systohc --utc")
        else:
          runAsChRoot("hwclock --systohc")

        print("Done!")
        #-------------------------------------------------------------


        #Set locale
        #-------------------------------------------------------------
        localeLs = "Note: use the down and up arrows to scroll this list and q to quit.\n"
        localeI = 0
        #FIXME: This uses tail which might not come by default with the installed system
        locales = runAsChRootAndGetOutput("tail --lines=+24 /etc/locale.gen | sed 's/#//g'").split("\n")

        for lI in range(locales.__len__()):
            if locales[lI] == "":
                locales.__delitem__(lI)

        for locale in locales:
            localeI += 1
            locale = locale.replace("\n", "").strip().split(" ")[0].strip()
            localeLs += str(localeI) + ") " + locale + "\n"

        while True:
            systemRun("echo '" + localeLs + "' | less")

            print("Please enter the number of the locale you wish to use(1-" + str(locales.__len__()) + "), or -2 to list the locales again!: ", end='')
            localeResponse = int(input())
            print()
            if localeResponse < -2: continue
            if localeResponse != -2: break

        localeAndEncoding = str(locales[localeResponse - 1]).replace("\n", "").strip()
        localeOnly = localeAndEncoding.split(" ")[0].strip()

        print("Setting locale to: " + localeOnly +" ...")
        runAsChRoot("echo LANG=" + localeOnly + " > /etc/locale.conf")
        #Reset locale.gen
        #FIXME: This uses hardcoded file sizes for resetting the files
        runAsChRoot("head -498 /etc/locale.gen > /etc/TempLocale.gen")
        runAsChRoot("cat /etc/TempLocale.gen > /etc/locale.gen")
        runAsChRoot("rm /etc/TempLocale.gen")

        #Append locale to locale.gen
        runAsChRoot("echo " + localeAndEncoding + " >> /etc/locale.gen")
        runAsChRoot("locale-gen")

        print("Done!")
        #-------------------------------------------------------------
        optionsDone[Options.configureLanguageAndTime] = True

        #-------------------------------------------------------------




    elif option == Options.personalizeSystem:
        #Set hostname, set root password, add additional users and set their password
        #-------------------------------------------------------------
        if not optionsDone[Options.mountPartitions]:
            print("Please mount the partitions first!")

        #Set hostname
        #-------------------------------------------------------------
        print("Please enter the hostname (ex: myPc): ", end='')
        hostName = str(input())
        print()

        runAsChRoot("echo " + hostName + " > /etc/hostname")
        #-------------------------------------------------------------



        #Set root password
        #-------------------------------------------------------------
        tries = 0
        print("Setting root password ...")
        while True:
            tries += 1
            passwordChangeResult = int(runAsChRoot("passwd root"))
            if tries > 4:
                print("Couldn't set root password, you'll have to set it yourself!")
                break
            if passwordChangeResult is 0: break
        #-------------------------------------------------------------


        #Add users
        #-------------------------------------------------------------
        #TODO: Add a way to remove users!!
        while True:
          print("Would you like to add an extra user? (y/n): ", end='')
          addUser = str(input())
          print()

          if addUser == "y":
              print("What's the name of the new user(lower case letters only)?: ", end = '')
              newUserName = str(input())
              print()

              print("would you like to give the use administrative privileges(sudo access)? (y/n): ", end = '')
              giveAccess = str(input())
              #TODO: Add a way for users to be able to select the shell that the new user will user and a way to install additional shells and auto completion for them
              if giveAccess == "y":
                  runAsChRoot("useradd -m -g users -G wheel,storage,power -s /bin/bash " + newUserName)
              else:
                  runAsChRoot("useradd -m -g users -G storage,power -s /bin/bash " + newUserName)

              print("Setting password for the new user ...")
              newUserTries = 0
              while True:
                  newUserTries += 1
                  newUserPasswordChangeResult = int(runAsChRoot("passwd " + newUserName))
                  if newUserTries > 4:
                      print("Couldn't set " + newUserName + "'s password, you'll have to set it yourself!")
                      break
                  if newUserPasswordChangeResult is 0: break
          else:
              break
        #-------------------------------------------------------------

        print("Done!")
        optionsDone[Options.personalizeSystem] = True
        #-------------------------------------------------------------




    elif option == Options.installExtra:
        #Install extra apps
        #-------------------------------------------------------------

        print("Would you like to install the networkmanager internet manager app? (y/n): ", end='')
        installNm = str(input())
        if installNm == "y":

            print("Would you like to install wifi capabilities to the target system? (y/n): ", end='')
            installWifi = str(input())
            print()

            runAsChRoot("pacman --noconfirm -S networkmanager dhcpcd")

            if installWifi == "y":
               runAsChRoot("pacman --noconfirm -S dialog iw wpa_supplicant ")
            runAsChRoot("systemctl enable NetworkManager")

        print("Done!")

        optionsDone[Options.installExtra] = True
        #-------------------------------------------------------------




    elif option == Options.installBootloader:
        #Install bootloader
        #-------------------------------------------------------------
        if not optionsDone[Options.mountPartitions]:
            print("Please mount the partitions first, before trying to install the bootloader on them!")
            continue

        print("Would you like to also install os-prober for detecting multiple oses(dualbooting)? (y/n): ", end='')
        useProber = str(input())
        print()


        print("Configuring initcpio ...")
        runAsChRoot("mkinitcpio -p linux")

        print("Installing bootloader ...")
        runAsChRoot("pacman --noconfirm -S grub")
        if useProber == "y":
            runAsChRoot("pacman --noconfirm -S os-prober")

        print("Configuring bootloader ...")
        if BOOT_MODE == 1:
            print("Installing grub for BIOS!!")
            listPartitions()

            print("Please enter the name of the disk where the bootloader should be installed (ex: /dev/sda): ", end='')
            bootloaderDisk = str(input())
            print()

            print("Are you sure you want to install the bootloader on the disk: " + bootloaderDisk + ", this will replace it's boot sector! (y = continue with the operation / n = take me back to the main menu without affecting anything): ", end='')
            sureOfInstall = str(input())
            print()

            if sureOfInstall != "y":
                continue

            runAsChRoot("grub-install --recheck --no-floppy " + bootloaderDisk)
            runAsChRoot("grub-mkconfig -o /boot/grub/grub.cfg")
        elif BOOT_MODE == 2:
            print("Installing grub for UEFI has not been implemented yet, sorry, you are going ot have to install grub on your own!!")
            continue

        print("Done!")
        optionsDone[Options.installBootloader] = True
        #-------------------------------------------------------------




    elif option == Options.chrootShell:
        #Enter an interactive shell of the system so the user can easily configure it
        #-------------------------------------------------------------
        print("Entering a chroot shell of the mounted system, so you can troubleshoot the system, or install stuff, or configure it more.")
        print("Note: Use the exit command to get back")
        systemRun("arch-chroot /mnt /bin/bash")
        optionsDone[Options.chrootShell] = True
        #-------------------------------------------------------------


exitInstaller()